require.config({
	paths:{
		jquery:'libs/jquery-1.11.3.min',
		underscore:'libs/underscore-min',
		Backbone:'libs/backbone-min',
		view:'view',
		template:'../template'
	}
});
require([
	'jquery','app'
	],function($,app){
	app.initialize();
});



