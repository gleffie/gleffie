define(['jquery','underscore','Backbone','view/LoadingPageView','view/dashBoardView','view/artistListView'],function(
	$, _,Backbone,LoadingPageView,DashBoardView,ArtistListView){
	
var router=Backbone.Router.extend({
	routes:{
		'dashBoard':'dashboardpage',
		'artistList':'artistlist',
		'':'loadingPage'
	}
});
var routerObject=new router();
var initialize=function(){
routerObject.on('route:dashboardpage',function(){
	var dashBoardView=new DashBoardView();
	dashBoardView.render();
	console.log('loading dashboardpage page');
});
routerObject.on('route:artistlist',function(){
	var artistListView=new ArtistListViewObject();
	artistListView.render();
	console.log('loading artist list page');
});	
routerObject.on('route:loadingPage',function(){
	console.log('loading page');
	//var userView=new UserView();
	//userView.render(
	var loadingPageTemp=new LoadingPageView;
	loadingPageTemp.render();
	//loadingPageTemp.initialize();
});
Backbone.history.start();
};
return {
	initialize : initialize 
};

});

