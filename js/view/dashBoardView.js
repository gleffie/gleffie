define(['jquery','underscore','Backbone','text!template/dashBoard.html'],
function($,_,backBone,dashBoardTemplate){
    var dashBoardView=Backbone.View.extend({
    	el:$('.wrapper'),
    	events:{
    		'click .artistContainer':'callUserList'
    	},
    	initialize:function(){
    		
    	},
    	render:function(){
    	 var template=_.template(dashBoardTemplate);	
    		this.$el.html(template);
    		window.location.href="index.html#dashboardpage";
    	},
    	callUserList:function(){
    		console.log('click of artist list');
    		window.location.href="index.html#artistlist";
    		callServiceRequest();
    	}
    });
    return 	dashBoardView;
    
    
});