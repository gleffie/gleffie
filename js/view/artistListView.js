define(['jquery','underscore','Backbone','text!template/dashBoard.html'],
function($,_,backBone,artistListTempate){
	var ArtistListViewObject=Backbone.View.extend({
		el:$('.wrapper'),
		initialize:function(){
			console.log('artist list init 1');
		},
		render:function(){
			 var template=_.template(artistListTempate);	
    		this.$el.html(template);
    		//window.location.href="index.html#artistDataPage";
		}
	});
	return ArtistListViewObject;
});