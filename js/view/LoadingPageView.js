define(['jquery','underscore','Backbone','text!template/loadingPage.html'],
function($,_,backBone,loadingPageTemplate){
	var loadingPageTemp=Backbone.View.extend({
		el:$('.wrapper'),
		$this:this,
		initialize:function(){
				
				},
		render:function(){
		//window.location.href="index.html/dashBoard";
			var template = _.template(loadingPageTemplate);
			this.$el.html(template);
			setTimeout(function(){
			window.location.href="index.html#dashBoard";				
			},5000);
			
		}
	});
	return loadingPageTemp;
	
});
