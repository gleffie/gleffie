﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Gleefie_MVC.Models;
using Gleefie_MVC.Helpers;
using System.Web.Script.Serialization;
using Gleefie_MVC.App_Start;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.Web.WebPages.OAuth;
using DotNetOpenAuth.AspNet;
using System.Collections.Generic;
using System.IO;

namespace Gleefie_MVC.Controllers
{
    public class ArtistController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ArtistController()
        {
        }
        public ArtistController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
       
        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetArtistMediaDetails(string emailId)
        {
            Artist model = new Artist();
            List<ArtistMediaVO> obj = new List<ArtistMediaVO>();
            var user = model.getArtistProfileDetails(emailId);
            if(user != null)
            {
                obj = model.getArtistMedia(user.ProfileId);
            }

            return Json(obj);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetArtistPaymentDetails(string emailId)
        {
            Artist model = new Artist();
            ArtistPaymentDetailsVO obj = new ArtistPaymentDetailsVO();
            var user = UserManager.FindByName(emailId);
            if (user != null)
            {
                obj = model.getArtistPaymentDetails(user.Id);
            }

            return Json(obj);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetArtistPerformanceDetails(string emailId)
        {
            Artist model = new Artist();
            ArtistPerformanceDetailsVO obj = new ArtistPerformanceDetailsVO();
            var user = UserManager.FindByName(emailId);
            if (user != null)
            {
                obj = model.getArtistPerformanceDetails(user.Id);
            }

            return Json(obj);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetArtistProfileDetails(string emailId)
        {
            Artist model = new Artist();
            ArtistProfileDetailsVO obj = new ArtistProfileDetailsVO();
            var user = UserManager.FindByName(emailId);
            if (user != null)
            {
                obj = model.getArtistProfileDetails(user.Id);
            }

            return Json(obj);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetArtistProfileList(Guid currentProfileId,bool bIsForward = true,int pageSize = 10)
        {
            try
            {
                Artist model = new Artist();
                List<ArtistProfileDetailsVO> obj = new List<ArtistProfileDetailsVO>();
                obj = model.getArtistProfileList(currentProfileId, bIsForward, pageSize);
                return Json(obj);
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdateArtistMediaDetails(ArtistMediaVO obj,string emailId)
        {
            try
            {
                Artist model = new Artist();
                var user = model.getArtistProfileDetails(emailId);
                if (user.ProfileId != obj.ProfileId)
                {
                    throw new Exception("you are not Authorized to perform this action");
                } 
                return Json(model.updateArtistMedia(obj));
            }
            catch(Exception ex)
            {
                return Json(false);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdateArtistPaymentDetails(ArtistPaymentDetailsVO obj, string emailId)
        {
            try
            {
                Artist model = new Artist();
                var user = model.getArtistProfileDetails(emailId);
                if (user.ProfileId != obj.ProfileId)
                {
                    throw new Exception("you are not Authorized to perform this action");
                }
                return Json(model.updateArtistPaymentDetails(obj));
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdateArtistPerformanceDetails(ArtistPerformanceDetailsVO obj, string emailId)
        {
            try
            {
                Artist model = new Artist();
                var user = model.getArtistProfileDetails(emailId);
                if (user.ProfileId != obj.ProfileId)
                {
                    throw new Exception("you are not Authorized to perform this action");
                }
                return Json(model.updateArtistPerformanceDetails(obj));
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdateArtistProfileDetails(ArtistProfileDetailsVO obj, string emailId)
        {
            try
            {
                Artist model = new Artist();
                var user = model.getArtistProfileDetails(emailId);
                if (user.ProfileId != obj.ProfileId)
                {
                    throw new Exception("you are not Authorized to perform this action");
                }
                return Json(model.updateArtistProfileDetails(obj));
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult InsertArtistMediaProfilePic(ArtistMediaVO obj,string UserId)
        {
            try
            {
                
                Artist model = new Artist();
                if (obj.ProfileId == null || obj.ProfileId == Guid.Empty || string.IsNullOrEmpty(UserId))
                {
                    throw new Exception("There is some technical issue, Please contact administrator");
                }
                obj.MediaType = 1; //ProfilePic
                obj.MeidaURL = UploadFiletoGleffie("",UserId).FirstOrDefault();
                return Json(model.insertArtistMedia(obj));
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }
     
        [HttpPost]
        [AllowAnonymous]
        public ActionResult InsertArtistVideoLinks(List<ArtistMediaVO> obj, string emailId)
        {
            try
            {
                if (obj == null)
                    return Json(false);

                Artist model = new Artist();
                var user = model.getArtistProfileDetails(emailId);
                if (user == null )
                {
                    throw new Exception("you are not Authorized to perform this action");
                }
                return Json(model.insertArtistMediaList(obj,user.ProfileId,2));
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult InsertArtistEventPics(string emailId)
        {
            try
            {
                List<ArtistMediaVO> objMedia = new List<ArtistMediaVO>();
                Artist model = new Artist();
                var user = model.getArtistProfileDetails(emailId);
                if (user == null )
                {
                    throw new Exception("you are not Authorized to perform this action");
                }
                var list = UploadFiletoGleffie(emailId);
                foreach (var item in list)
                {
                    var artismedia = new ArtistMediaVO();
                    artismedia.MeidaURL = item;
                    objMedia.Add(artismedia);
                }
                return Json(model.insertArtistMediaList(objMedia, user.ProfileId, 3));
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult InsertArtistPaymentDetails(ArtistPaymentDetailsVO obj, string emailId)
        {
            try
            {
                Artist model = new Artist();
                var user = model.getArtistProfileDetails(emailId);
                if (user == null)
                {
                    throw new Exception("you are not Authorized to perform this action");
                }
                obj.ProfileId = user.ProfileId;
                return Json(model.insertArtistPaymentDetails(obj));
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult InsertArtistPerformanceDetails(ArtistPerformanceDetailsVO obj, string emailId)
        {
            try
            {
                Artist model = new Artist();
                var user = model.getArtistProfileDetails(emailId);
                if (user == null)
                {
                    throw new Exception("you are not Authorized to perform this action");
                }
                obj.ProfileId = user.ProfileId;
                return Json(model.insertArtistPerformanceDetails(obj));
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult InsertArtistProfileDetails(ArtistProfileDetailsVO obj)
        {
            try
            {
                Artist model = new Artist();
                if (string.IsNullOrEmpty(obj.UserId))
                {
                    throw new Exception("you are not Authorized to perform this action");
                }
                bool status = model.insertArtistProfileDetails(obj);
                if(status)
                {
                    ArtistMediaVO objmedia  = new ArtistMediaVO();
                    objmedia.ProfileId = obj.ProfileId;
                    this.InsertArtistMediaProfilePic(objmedia,obj.UserId);
                }
                return Json(status);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }


        //[HttpPost]
        //[AllowAnonymous]
        //public JsonResult UploadFile(string emailId)
        //{
        //    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
        //    {
        //        // Get the uploaded file from the Files collection
        //        var httpPostedFile = System.Web.HttpContext.Current.Request.Files["UploadedFile"];

        //        try
        //        {
        //            if (httpPostedFile != null)
        //            {
        //                // Validate the uploaded image(optional)
        //                string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/UploadedFiles/") + emailId + "_" + DateTime.Now.ToString() + "/";

        //                //Directory not exists then create new directory
        //                if (!Directory.Exists(folderPath))
        //                {
        //                    Directory.CreateDirectory(folderPath);
        //                }

        //                // Get the complete file path
        //                var fileSavePath = Path.Combine(folderPath, httpPostedFile.FileName);
        //                // Save the uploaded file to "UploadedFiles" folder
        //                httpPostedFile.SaveAs(fileSavePath);

        //                var artist = new Artist();
        //                artist.updateProfileLink(emailId, fileSavePath);
        //            }
        //            return Json(true);
        //        }
        //        catch (Exception ex)
        //        {
        //            return Json(false);
        //        }
        //    }
        //    else
        //    {
        //        return Json(false);
        //    }
        //}

        [HttpGet]
        public ActionResult DeleteArtistMedia(Guid mediaId, string emailId)
        {
            Artist model = new Artist();
            var user = UserManager.FindByName(emailId);
            if (user != null)
            {
                string url = model.deleteArtistMedia(mediaId);
                if (!string.IsNullOrEmpty(url))
                {
                    if (System.IO.File.Exists(url))
                    {
                        System.IO.File.Delete(url);
                    }
                }
            }
            return new EmptyResult();
        }
        public List<string> UploadFiletoGleffie(string emailId,string UserId = "")
        {
            List<string> fileSavePath = new List<string>();
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the uploaded file from the Files collection
                HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

                try
                {
                    if (hfc != null)
                    {
                        // Validate the uploaded image(optional)
                        if (string.IsNullOrEmpty(UserId))
                        {
                            var user = UserManager.FindByName(emailId);
                            UserId = user.Id;
                        }
                        string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/UploadedFiles/") + UserId + "/";

                        //Directory not exists then create new directory
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }

                        // Get the complete file path
                        foreach (HttpPostedFile item in hfc)
                        {
                            var res = Path.Combine(folderPath, item.FileName);
                            fileSavePath.Add(res);
                            // Save the uploaded file to "UploadedFiles" folder
                            item.SaveAs(res);
                        }

                    }
                    return fileSavePath;
                }
                catch (Exception ex)
                {
                    return fileSavePath;
                }
            }
            else
            {
                return fileSavePath;
            }
        }
    }
}