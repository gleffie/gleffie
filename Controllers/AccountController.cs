﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Gleefie_MVC.Models;
using Gleefie_MVC.Helpers;
using System.Web.Script.Serialization;
using Gleefie_MVC.App_Start;
using Microsoft.Owin.Host.SystemWeb;
using Gleefie_MVC.Enums;
using System.IO;
using Microsoft.Web.WebPages.OAuth;
using DotNetOpenAuth.AspNet;


namespace Gleefie_MVC.Controllers
{
    [RequireHttps]
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }
        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
       
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModelVO model)
        {
            ResultObject res = new ResultObject();
            if (!ModelState.IsValid)
            {
                res.error.Add("Please enter all the required fields.");
                res.result = false;
                return Json(res);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var user = UserManager.FindByEmail(model.EmailAddress);
            if (user == null)
            {
                res.result = false;
                res.error.Add("User is not registered.");
                return Json(res);
            }
            var result = SignInManager.PasswordSignIn(user.UserName, model.Password, model.RememberMe, shouldLockout: false);
           if (result == SignInStatus.Success)
           {
               res.result = true;
               res.error = null;
               res.UserName = user.UserName;
               return Json(res);
           }
           else
           {
               res.result = false;
               res.error.Add("Invalid details. Please try again");
               return Json(res);
           }
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModelVO model)
        {
            var res = new ResultObject();
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.UserName, Email = model.EmailAddress,imagePath=model.imagePath};
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {  
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    try
                    {
                        var enumtype = ((RoleEnum)model.RoleTypeId).ToString();
                        await UserManager.AddToRoleAsync(user.Id, enumtype);
                        if(model.ArtistDetails != null && model.RoleTypeId == (int)RoleEnum.Artist)
                        {
                            model.ArtistDetails.UserId = user.Id;
                            ArtistController AC = new ArtistController();
                            AC.InsertArtistProfileDetails(model.ArtistDetails);
                            
                        }
                        res.result = true;
                        res.error = null;
                        res.UserName = user.UserName;
                        return Json(res);
                    }
                    catch(Exception ex)
                    {
                        res.result = false;
                        res.error.Add("There is some technical Issue");
                        return Json(res);
                    }
                }
                else
                {
                    res.result = false;
                    res.error = (result.Errors.ToList());
                    return Json(res);
                }
            }
            return Json(res);
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(LoginModelVO model)
        {
            ResultObject res = new ResultObject();
            if (ModelState.IsValid)
            {
                var user = UserManager.FindByNameAsync(model.EmailAddress).Result;
                if (user == null)
                {
                    res.result = false;
                    res.error.Add("User is not registered");
                }
                else 
                {
                    var token = UserManager.GeneratePasswordResetToken(user.Id);
                    var result = UserManager.ResetPassword(user.Id, token, model.Password);
                    res.result = result.Succeeded;
                    res.UserName = user.UserName;
                    res.error = result.Errors.ToList();
                }
                return Json(res);
            }

            return Json(model);
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public  ActionResult ChangePassword(ResetPasswordViewModel model)
        {
            var res = new ResultObject();
            res.result = false;
            if (!ModelState.IsValid)
            {
                res.result = false;
                res.error.Add("Please enter mandatory fields");
                return Json(res);
            }
            var user = UserManager.FindByName(model.Email);
            if (user == null)
            {
                res.result = false;
                res.error.Add("User is not registered.");
                return Json(res);
            }
            var result =  UserManager.ResetPassword(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                res.result = true;
                res.error = result.Errors.ToList();
                res.UserName = user.UserName;
                return Json(res);
            }
            return Json(res);
        }

        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return Json(new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account")));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            var res = new ResultObject();
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback"));
            if (!result.IsSuccessful)
            {
                res.result = false;
                res.error.Add(result.Error.ToString());
                res.UserName = result.UserName;
                return Json(res);
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false) || User.Identity.IsAuthenticated)
            {
                res.result = true;
                res.error = null;
                res.UserName = result.UserName;
                return Json(res);
            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModelVO { UserName = result.UserName, ExternalLoginData = loginData });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModelVO model)
        {
            string provider = null;
            string providerUserId = null;
            var res = new ResultObject();

            if (ModelState.IsValid)
            {
                // Insert a new user into the database
                using (Context db = new Context())
                {
                    UserProfileVO user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                    // Check if user already exists
                    if (user == null)
                    {
                        // Insert name into the profile table
                        db.UserProfiles.Add(new UserProfileVO { UserName = model.UserName });
                        db.SaveChanges();

                        OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                        OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);
                        res.result = true;
                        res.error = null;
                    }
                    else
                    {   res.result = false;
                        res.error.Add("User name already exists. Please enter a different user name.");
                    }
                }
            }

            res.UserName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
            
            return Json(res);
        }


        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return Json(true);
        }

       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}
