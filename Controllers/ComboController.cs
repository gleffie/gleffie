﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gleefie_MVC.Helpers;
using Gleefie_MVC.Models;

namespace Gleefie_MVC.Controllers
{
    public class ComboController : Controller
    {
        [HttpGet]
        public ActionResult GetCategories()
        {
            Combos objCombo = new Combos();
            return Json(objCombo.GetCategories());
        }

        [HttpGet]
        public ActionResult GetCities(int stateId)
        {
            Combos objCombo = new Combos();
            return Json(objCombo.GetCities(stateId));
        }

        [HttpGet]
        public ActionResult GetCountries()
        {
            Combos objCombo = new Combos();
            return Json(objCombo.GetCountries());
        }

        [HttpGet]
        public ActionResult GetEventTypes()
        {
            Combos objCombo = new Combos();
            return Json(objCombo.GetEventTypes());
        }

        [HttpGet]
        public ActionResult GetLanguages()
        {
            Combos objCombo = new Combos();
            return Json(objCombo.GetLanguages());
        }

        [HttpGet]
        public ActionResult GetMediaTypes()
        {
            Combos objCombo = new Combos();
            return Json(objCombo.GetMediaTypes());
        }

        [HttpGet]
        public ActionResult GetPricings()
        {
            Combos objCombo = new Combos();
            return Json(objCombo.GetPricings());
        }

        [HttpGet]
        public ActionResult GetPricings(int CountryId)
        {
            Combos objCombo = new Combos();
            return Json(objCombo.GetStates(CountryId));
        }

        [HttpGet]
        public ActionResult GetSubCategories(int categoryId)
        {
            Combos objCombo = new Combos();
            return Json(objCombo.GetSubCategories(categoryId));
        }        
    }
}