﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using Gleefie_MVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Owin;
using Gleefie_MVC.App_Start;

namespace Gleefie_MVC
{
    public partial class Startup
    {
        public static void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(Context.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity =
                        SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(TimeSpan.FromMinutes(30),
                            (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
             app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
             app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            app.UseGoogleAuthentication(clientId: "1010387764077-tr9lfuc46t6j1osh4fl5mpnelu9sri6j.apps.googleusercontent.com", clientSecret: "njTJlrqIxLUM7aWKYc-OuhJU");
            app.UseFacebookAuthentication(appId: "880420785371357", appSecret: "f1dda69c0a306d6a644351bebf3f28c6");
            //var facebookAuthenticationOptions = new FacebookAuthenticationOptions()
            //{
            //    AppId = "880420785371357",
            //    AppSecret = "f1dda69c0a306d6a644351bebf3f28c6"
            //};
            //facebookAuthenticationOptions.Scope.Add("email");
            //facebookAuthenticationOptions.Scope.Add("public_profile");
            //app.UseFacebookAuthentication(facebookAuthenticationOptions);
        }       
    }
}
