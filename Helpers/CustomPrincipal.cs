﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using Gleefie_MVC.Models;

namespace Gleefie_MVC.Helpers
{
    public class CustomPrincipal:IPrincipal
    {
        public CustomPrincipal(IIdentity identity)
        {
            Identity = identity;
        }

        public IIdentity Identity
        {
            get;
            private set;
        }

        public UserProfileVO User { get; set; }

        public bool IsInRole(string role)
        {
            return true;
        }
    }
}