﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using Gleefie_MVC.Models;
using Gleefie_MVC.Enums;
using System.Data.SqlClient;


namespace Gleefie_MVC.Helpers
{
    public class Artist
    {
        public ArtistMediaVO getArtistMedia(string UserId)
        {
            ArtistMediaVO objArtistMeida = new ArtistMediaVO();
            using(Context dbContext = new Context())
            {
                objArtistMeida = (from a in dbContext.ArtistMediaType.Include("profileobj").Include("MediaTypeObj").
                                           Where(a => a.profileobj.UserId.Equals(UserId) && a.ProfileId.Equals(a.profileobj.ProfileId))
                              select a      
                             ).FirstOrDefault();
                objArtistMeida.profileobj.ContactNumber = string.Empty;
            }
            return objArtistMeida;
        }
        public List<ArtistMediaVO> getArtistMedia(Guid profileID)
        {
            try
            {
                if (profileID == Guid.Empty)
                    return null;
                List<ArtistMediaVO> objArtistMeida = new List<ArtistMediaVO>();
                using (Context dbContext = new Context())
                {
                    objArtistMeida = (from a in dbContext.ArtistMediaType.Include("MediaTypeObj").
                                               Where(a => a.ProfileId.Equals(profileID))
                                      select a
                                 ).ToList();
                }
                return objArtistMeida;
            }
            catch(Exception ex)
            {
                return null;
            }
            
        }
        public ArtistPaymentDetailsVO getArtistPaymentDetails(string UserId)
        {
            ArtistPaymentDetailsVO obj = new ArtistPaymentDetailsVO();
            using (Context dbContext = new Context())
            {
                obj = (from a in dbContext.ArtistPaymentDetails.Include("profileobj").
                                          Where(a => a.profileobj.UserId.Equals(UserId) && a.ProfileId.Equals(a.profileobj.ProfileId))
                              select a
                             ).FirstOrDefault();
                obj.profileobj.ContactNumber = string.Empty;
            }
            return obj;
        }
        public ArtistPerformanceDetailsVO getArtistPerformanceDetails(string UserId)

        {
            ArtistPerformanceDetailsVO obj = new ArtistPerformanceDetailsVO();
            using (Context dbContext = new Context())
            {
                obj = (from a in dbContext.ArtistPerformanceDetailss.Include("profileobj").Include("MediaTypeObj").
                                           Where(a => a.profileobj.UserId.Equals(UserId) && a.ProfileId.Equals(a.profileobj.ProfileId))
                                  select a
                             ).FirstOrDefault();
                obj.profileobj.ContactNumber = string.Empty;
            }
            return obj;
        }
        public ArtistProfileDetailsVO getArtistProfileDetails(string UserId)
        {
            ArtistProfileDetailsVO obj = new ArtistProfileDetailsVO();
            using (Context dbContext = new Context())
            {
                obj = (from a in dbContext.ArtistProfileDetails.
                                          Where(a => a.UserId.Equals(UserId))
                       select a
                             ).FirstOrDefault();
                var list = getArtistMedia(obj.ProfileId);
                
                if (list != null)
                     obj.artistMediaList = list;
                
                obj.ContactNumber = string.Empty;
            }
            return obj;
        }
        public List<ArtistProfileDetailsVO> getArtistProfileList(Guid currentProfileId, bool bIsForward = true, int PageSize = 10)
        {
            List<ArtistProfileDetailsVO> objList = new List<ArtistProfileDetailsVO>();

            if (currentProfileId == Guid.Empty)
                currentProfileId = new Guid();

            try
            {
                using (Context dbContext = new Context())
                {
                    var id = new SqlParameter("@CurrentId", currentProfileId);
                    var bForward = new SqlParameter("@bIsForward", bIsForward);
                    var recordpageSize = new SqlParameter("@PageSize", PageSize);

                    var result = dbContext.Database.SqlQuery<ArtistProfileDetailsVO>("GetArtistList @CurrentId, @bIsForward, @PageSize", id, bIsForward, recordpageSize).ToList();
                    return result;
                }

            }
            catch (Exception ex)
            {
                return objList;
            }

        }

        public bool updateArtistMedia(ArtistMediaVO obj)
        {
            try
            {
                if (obj == null)
                    return false;

                using (Context dbContext = new Context())
                {
                    var record = (from a in dbContext.ArtistMediaType.Include("MediaTypeObj").
                                           Where(a => a.MediaId.Equals(obj.MediaId))
                                  select a
                             ).FirstOrDefault();

                    record.MeidaURL = obj.MeidaURL;
                    record.MediaType = obj.MediaType;
                    record.modified = DateTime.Now;

                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
            }

        }
        public bool updateArtistMediaList(List<ArtistMediaVO> obj)
        {
            try
            {
                if (obj == null)
                    return false;

                using (Context dbContext = new Context())
                {
                    foreach (var item in obj)
                    {
                        var record = (from a in dbContext.ArtistMediaType.Include("MediaTypeObj").
                                               Where(a => a.MediaId.Equals(item.MediaId))
                                      select a
                                 ).FirstOrDefault();

                        record.MeidaURL = item.MeidaURL;
                        record.MediaType = item.MediaType;
                        record.modified = DateTime.Now;

                        dbContext.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool updateArtistPaymentDetails(ArtistPaymentDetailsVO obj)
        {
            try
            {
                if (obj == null)
                    return false;

                using (Context dbContext = new Context())
                {
                    var record = (from a in dbContext.ArtistPaymentDetails.Where(a => a.Id.Equals(obj.Id)) select a).FirstOrDefault();
                    record.AccountHolderName = obj.AccountHolderName;
                    record.AccountNumber = obj.AccountNumber;
                    record.BankAddress = obj.BankAddress;
                    record.BankName = obj.BankName;
                    record.IFSCCode = obj.IFSCCode;
                    record.MICRCode = obj.MICRCode;
                    record.PanNumber = obj.PanNumber;
                    record.modified = DateTime.Now;
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool updateArtistPerformanceDetails(ArtistPerformanceDetailsVO obj)
        {
            try
            {
                if (obj == null)
                    return false;
                using (Context dbContext = new Context())
                {
                    var record = (dbContext.ArtistPerformanceDetailss.Where(a => a.Id.Equals(obj.Id))).FirstOrDefault();
                    record.AvailableOnRequest = obj.AvailableOnRequest;
                    record.Comments = obj.Comments;
                    record.IsReady = obj.IsReady;
                    record.Languages = obj.Languages;
                    record.MaxValue = obj.MaxValue;
                    record.MinValue = obj.MinValue;
                    record.PerformanceDur = obj.PerformanceDur;
                    record.Pricing = obj.Pricing;
                    record.modified = DateTime.Now;
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        public bool updateArtistProfileDetails(ArtistProfileDetailsVO obj)
        {
            try
            {

                if (obj != null)
                    return false;
                using (Context dbContext = new Context())
                {
                    var record = (dbContext.ArtistProfileDetails.Where(a => a.ProfileId.Equals(obj.ProfileId)).FirstOrDefault());
                    record.AboutMe = obj.AboutMe;
                    record.Category = obj.Category;
                    record.City = obj.City;
                    record.ContactNumber = obj.ContactNumber;
                    record.ContactPerson = obj.ContactPerson;
                    record.Gender = obj.Gender;
                    record.GroupMembers = obj.GroupMembers;
                    record.GroupStrength = obj.GroupStrength;
                    record.OtherCategory = obj.OtherCategory;
                    record.ProfessionName = obj.ProfessionName;
                    record.modified = DateTime.Now;
                    dbContext.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public bool updateProfileLink(string emailId, string ImagePath)
        //{
        //    using (Context dbContext = new Context())
        //    {
        //        var user = dbContext.UserProfiles.Where(a => a.EmailAddress == emailId).FirstOrDefault();
        //        if (user != null)
        //        {
        //            user.imagePath = ImagePath;
        //            user.Modified = DateTime.Now;
        //            dbContext.SaveChanges();
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //}

        public bool insertArtistMedia(ArtistMediaVO obj)
        {
            try
            {
                if (obj == null)
                    return false;

                using (Context dbContext = new Context())
                {
                    obj.created = DateTime.Now;
                    obj.modified = DateTime.Now;
                    dbContext.ArtistMediaType.Add(obj);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool insertArtistMediaList(List<ArtistMediaVO> obj,Guid profileId,int mediaType)
        {
            try
            {
                if (obj == null)
                    return false;

                using (Context dbContext = new Context())
                {
                    foreach (var item in obj)
                    {
                        item.created = DateTime.Now;
                        item.modified = DateTime.Now;
                        item.ProfileId = profileId;
                        item.MediaType = mediaType;
                        dbContext.ArtistMediaType.Add(item);
                        dbContext.SaveChanges();
                        
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool insertArtistPaymentDetails(ArtistPaymentDetailsVO obj)
        {
            try
            {
                if (obj == null)
                    return false;

                using (Context dbContext = new Context())
                {
                    obj.created = DateTime.Now;
                    obj.modified = DateTime.Now;
                    dbContext.ArtistPaymentDetails.Add(obj);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool insertArtistPerformanceDetails(ArtistPerformanceDetailsVO obj)
        {
            try
            {
                if (obj == null)
                    return false;
                using (Context dbContext = new Context())
                {
                    obj.created = DateTime.Now;
                    obj.modified = DateTime.Now;
                    dbContext.ArtistPerformanceDetailss.Add(obj);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool insertArtistProfileDetails(ArtistProfileDetailsVO obj)
        {
            try
            {

                if (obj != null)
                    return false;
                using (Context dbContext = new Context())
                {
                    obj.created = DateTime.Now;
                    obj.modified = DateTime.Now;
                    dbContext.ArtistProfileDetails.Add(obj);
                    dbContext.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string deleteArtistMedia(Guid mediaId)
        {
            try
            {
                if (mediaId == Guid.Empty)
                {

                    using (Context dbContext = new Context())
                    {
                        var record = dbContext.ArtistMediaType.Where(a => a.MediaId == mediaId).FirstOrDefault();
                        dbContext.ArtistMediaType.Remove(record);
                        dbContext.SaveChanges();
                        return record.MeidaURL;
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }
    }
}