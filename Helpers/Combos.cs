﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using Gleefie_MVC.Models;
using Gleefie_MVC.Enums;
using System.Reflection;
using System.ComponentModel;

namespace Gleefie_MVC.Helpers
{
    public class Combos
    {
        public List<CategoryTypeVO> GetCategories()
        {
            using (Context dbContext = new Context())
            {
                return dbContext.Categories.ToList();

            }
        }
        public List<CountryVO>  GetCountries()
        {
            using (Context dbContext = new Context())
            {
                return dbContext.Countries.ToList();
            }
        }
        public List<CityVO> GetCities(int stateid = 0)
        {
            using (Context dbContext = new Context())
            {
                if(stateid == 0)
                    return dbContext.Cities.ToList();
                else
                    return dbContext.Cities.Where(a => a.StateId == stateid).ToList();
            }
        }
        public List<EventTypeVO> GetEventTypes()
        {
            using (Context dbContext = new Context())
            {
                return dbContext.Events.ToList();

            }
        }    
        public List<LanguagesVO> GetLanguages()
        {
            using (Context dbContext = new Context())
            {
                return dbContext.Languages.ToList();

            }
        }
        public List<MediaTypeVO> GetMediaTypes()
        {
            using (Context dbContext = new Context())
            {
                return dbContext.MediaTypes.ToList();

            }
        }
        public List<PricingTypeVO> GetPricings()
        {
            using (Context dbContext = new Context())
            {
                return dbContext.Pricings.ToList();

            }
        }
        public List<StatesVO> GetStates(int countryId = 0)
        {
            using (Context dbContext = new Context())
            {
                if (countryId == 0)
                    return dbContext.States.ToList();
                else
                    return dbContext.States.Where(a => a.CountryId == countryId).ToList();
            }
        }      
        public List<SubCategoryTypeVO> GetSubCategories(int categoryId =0)
        {
            using (Context dbContext = new Context())
            {
                if(categoryId == 0)
                    return dbContext.SubCategories.ToList();
                else
                    return dbContext.SubCategories.Where(a=> a.Categorytype==categoryId).ToList();

            }
        }
    }
}