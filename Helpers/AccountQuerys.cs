﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using Gleefie_MVC.Models;
using Gleefie_MVC.Enums;
using System.Reflection;
using System.ComponentModel;

namespace Gleefie_MVC.Helpers
{
    public class AccountQuerys
    {
        public string GetUserNameByEmail(string email)
        {
            using (var context = new Context())
            {
                var res = context.UserProfiles.Where(a => a.EmailAddress.Equals(email)).FirstOrDefault();
                if (res != null)
                    return res.UserName;
                else
                    return "";
            }
        }

        public static string EnumTypeDescription(Enum enumType)
        {
            FieldInfo fi = enumType.GetType().GetField(enumType.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return enumType.ToString();
            }
        }
        public static UserProfileVO GetUserByEmail(string email)
        {
            using (var context = new Context())
            {
                var res = context.UserProfiles.Where(a => a.EmailAddress.Equals(email)).FirstOrDefault();
                if (res != null)
                    return res;
                else
                    return null;
            }
        }
    }

    public static class UserManager
    {
        /// <summary>
        /// Returns the User from the Context.User.Identity by decrypting the forms auth ticket and returning the user object.
        /// </summary>
        public static UserProfileVO User
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    var user = HttpContext.Current.User;
                    var userProfile = new UserProfileVO();
                    userProfile.EmailAddress = user.Identity.Name;
                    userProfile.UserName = GetUserNameByEmail(user.Identity.Name);
                    // The user is authenticated. Return the user from the forms auth ticket.
                    return userProfile;
                }
                else if (HttpContext.Current.Items.Contains("User"))
                {
                    // The user is not authenticated, but has successfully logged in.
                    return (UserProfileVO)HttpContext.Current.Items["User"];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Authenticates a user against a database, web service, etc.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <returns>User</returns>
        public static UserProfileVO AuthenticateUser(string email, string password)
        {
            // Lookup user in database, web service, etc. We'll just generate a fake user for this demo.
            using (var context = new Context())
            {
                var res = context.UserProfiles.Where(a => a.EmailAddress.Equals(email)).FirstOrDefault();
                if (res != null)
                    return res;
                else
                    return null;
            }            
        }

        /// <summary>
        /// Authenticates a user via the MembershipProvider and creates the associated forms authentication ticket.
        /// </summary>
        /// <param name="logon">Logon</param>
        /// <param name="response">HttpResponseBase</param>
        /// <returns>bool</returns>
        public static bool ValidateUser(LoginModelVO logon, HttpResponseBase response)
        {
            bool result = false;

            if (Membership.ValidateUser(logon.EmailAddress, logon.Password))
            {
                // Create the authentication ticket with custom user data.
                if (logon.RememberMe)
                {
                    var serializer = new JavaScriptSerializer();
                    string userData = serializer.Serialize(UserManager.User);

                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                            logon.EmailAddress,
                            DateTime.Now,
                            DateTime.Now.AddDays(30),
                            true,
                            userData,
                            FormsAuthentication.FormsCookiePath);

                    // Encrypt the ticket.
                    string encTicket = FormsAuthentication.Encrypt(ticket);

                    // Create the cookie.
                    response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

                }
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Clears the user session, clears the forms auth ticket, expires the forms auth cookie.
        /// </summary>
        /// <param name="session">HttpSessionStateBase</param>
        /// <param name="response">HttpResponseBase</param>
        public static void Logoff(HttpSessionStateBase session, HttpResponseBase response)
        {
            // Delete the user details from cache.
            session.Abandon();

            // Delete the authentication ticket and sign out.
            FormsAuthentication.SignOut();

            // Clear authentication cookie.
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            response.Cookies.Add(cookie);
        }

        public static string GetUserNameByEmail(string email)
        {
            using (var context = new Context())
            {
                var res = context.UserProfiles.Where(a => a.EmailAddress.Equals(email)).FirstOrDefault();
                if (res != null)
                    return res.UserName;
                else
                    return "";
            }
        }
    }
}