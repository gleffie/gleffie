﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;


namespace Gleefie_MVC.Models
{
    [Table("States")]
    public class StatesVO
    {
        [Key()]
        public int Id { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }
        public bool? isActive { get; set; }
       
        [ForeignKey("CountryId")]
        public CountryVO Country { get; set; }
        public ICollection<CityVO> Cities { get; set; }

    }
}