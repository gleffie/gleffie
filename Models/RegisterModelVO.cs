﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
namespace Gleefie_MVC.Models
{
    [Table("UserProfile")]
    public class RegisterModelVO
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string EmailAddress { get; set; }
        
        [NotMapped]
        public string FirstName { get; set; }
        
        [NotMapped]
        public string LastName { get; set; }
                
        [NotMapped]
        public string PhoneNumber { get; set; }
        
        [NotMapped]
        public bool isActive { get; set; }
        [NotMapped]
        public DateTime created { get; set; }
        
        [NotMapped]
        public DateTime Modified { get; set; }
        public string imagePath { get; set; }
        public int RoleTypeId { get; set; }
        
        [ForeignKey("RoleTypeId")]
        public virtual RolesVO Role { get; set; }
        public ArtistProfileDetailsVO ArtistDetails { get; set; }
    }
}