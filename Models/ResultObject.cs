﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gleefie_MVC.Models
{
    public class ResultObject
    {
        public bool result { get; set; }
        public List<string> error { get; set; }
        public string UserName { get; set; }
    }
}