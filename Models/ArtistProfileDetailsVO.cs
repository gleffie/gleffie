﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{
    [Table("ArtistProfileDetails")]
    public class ArtistProfileDetailsVO
    {
        public ArtistProfileDetailsVO()
        {
        }

        [Key()]
        public Guid ProfileId { get; set; }
        public string ProfessionName { get; set; }
        public char Gender { get; set; }
        public int City { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNumber { get; set; }
        public int GroupStrength { get; set; }
        public string UserId { get; set; }
        public string GroupMembers { get; set; }
        public string SubCategory { get; set; }
        public string OtherCategory { get; set; }
        public int Category { get; set; }
        public string GeoCode { get; set; }

        [ForeignKey("Category")]
        public CategoryTypeVO Categoryobj { get; set; }
        public string AboutMe { get; set; }
        public DateTime? created { get; set; }
        public DateTime? modified { get; set; }

        [NotMapped]
        public List<ArtistMediaVO> artistMediaList { get; set; }
        
        [NotMapped]
        public ArtistPaymentDetailsVO artistPaymentDetails { get; set; }
        
        [NotMapped]
        public ArtistPerformanceDetailsVO artistPerformanceDetails { get; set; }
        
        [NotMapped]
        public ArtistProfileDetailsVO artistProfileDetails { get; set; }
    }
}