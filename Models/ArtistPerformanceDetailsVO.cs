﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{
    [Table("ArtistPerformanceDetails")]
    public class ArtistPerformanceDetailsVO
    {
        public ArtistPerformanceDetailsVO()
        {
        }

        [Key()]
        public int Id { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }
        public bool AvailableOnRequest { get; set; }
        public int Pricing { get; set; }
        public string Comments { get; set; }
        public string TypeOfEvents { get; set; }
        public string Languages { get; set; }
        public string PerformanceDur { get; set; }
        public bool IsReady { get; set; }
        public Guid ProfileId { get; set; }

        [ForeignKey("ProfileId")]
        public virtual ArtistProfileDetailsVO profileobj { get; set; }
        public DateTime? created { get; set; }
        public DateTime? modified { get; set; }
    }
}