﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;


namespace Gleefie_MVC.Models
{
    [Table("Pubs")]
    public class PubsVO
    {
        public PubsVO()
        {
        }

        [Key()]
        public int Id { get; set; }
        public string PubName { get; set; }
        public int? PubRating { get; set; }
        public bool? isActive { get; set; }
        public int? PubTypeId { get; set; }
        public virtual PubTypeVO pubType { get; set; }
        public DateTime? Created { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? Modified { get; set; }
        public Guid? ModifiedBy { get; set; }
        public ICollection<PubAddressesVO> PubAddress { get; set; }
    }
}