﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;


namespace Gleefie_MVC.Models
{
    [Table("Roles")]
    public class RolesVO
    {
        [Key()]
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public bool? isActive { get; set; }
        public DateTime? Created { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? ModifiedBy { get; set; }

        public virtual ICollection<PermissionsVO> Permissions { get; set; }
    }
}