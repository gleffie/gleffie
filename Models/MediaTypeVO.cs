﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace Gleefie_MVC.Models
{
     [Table("MediaType")]
    public class MediaTypeVO
    {
        public MediaTypeVO()
        {
        }

        [Key()]
        public int Id { get; set; }
        public string MediaType { get; set; }
        public string MediaDescription { get; set; }
        public bool? isActive { get; set; }
    }
}