﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Gleefie_MVC.Models
{
    [Table("UserProfile")]
    public class UserProfileVO
    {
        [Key]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string imagePath { get; set; }
        public bool isActive { get; set; }
        public DateTime created { get; set; }
        public DateTime Modified { get; set; }
    }
}