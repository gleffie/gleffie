﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;


namespace Gleefie_MVC.Models
{
    [Table("PubAddresses")]
    public class PubAddressesVO
    {
        public PubAddressesVO()
        {
            
        }
        [Key()]
        public int Id { get; set; }
        public int CityId { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PinCode { get; set; }
        public string MobileNumber { get; set; }
        public string LandLineNumber { get; set; }
        public string PubZeoCode { get; set; }
        public int PubId { get; set; }

        [ForeignKey("PubId")]
        public PubsVO pubs { get; set; }
    }
}