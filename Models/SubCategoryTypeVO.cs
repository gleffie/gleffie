﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{
    [Table("SubCategoryType")]
    public class SubCategoryTypeVO
    {
        public SubCategoryTypeVO()
        {
        }

        [Key()]
        public int Id { get; set; }
        public string SubCategoryType { get; set; }
        public string Description { get; set; }
        public int Categorytype { get; set; }

        [ForeignKey("Categorytype")]
        public virtual CategoryTypeVO CategoryType{ get; set; }
        public bool? isActive { get; set; }
    }
}