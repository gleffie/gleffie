﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{
    [Table("PricingType")]
    public class PricingTypeVO
    {
        public PricingTypeVO()
        {
        }

        [Key()]
        public int Id { get; set; }
        public string Pricing { get; set; }     
        public bool? isActive { get; set; }
    }
}