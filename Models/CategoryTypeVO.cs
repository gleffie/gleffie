﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{
    [Table("CategoryType")]
    public class CategoryTypeVO
    {
        public CategoryTypeVO()
        {
        }

        [Key()]
        public int Id { get; set; }
        public string CategoryType { get; set; }
        public string CategoryDescription { get; set; }
        public bool? isActive { get; set; }
    }
}