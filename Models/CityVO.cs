﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{
    [Table("Cities")]
    public class CityVO
    {
        public CityVO()
        {
        }

        [Key()]
        public int Id { get; set; }
        public string CityName { get; set; }
        public int StateId { get; set; }
        public bool? isActive { get; set; }
       
        [ForeignKey("StateId")]
        public StatesVO State { get; set; }

    }
}