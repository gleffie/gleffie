﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;


namespace Gleefie_MVC.Models
{
     [Table("PubTypes")]
    public class PubTypeVO
    {
        public PubTypeVO()
        {
        }

        [Key()]
        public int id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public bool? isActive { get; set; }
    }
}