﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{
    [Table("EventType")]
    public class EventTypeVO
    {
        public EventTypeVO()
        {
        }

        [Key()]
        public int Id { get; set; }
        public string EventType { get; set; }
        public string Description { get; set; }
        public bool? isActive { get; set; }
    }
}