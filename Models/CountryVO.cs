﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace Gleefie_MVC.Models
{
    [Table("Countries")]
    public class CountryVO
    {
        public CountryVO()
        {

        }
        [Key()]
        public int Id { get; set; }
        public string CountryName { get; set; }
        public bool? isActive { get; set; }
        public ICollection<StatesVO> States { get; set; }

    }
}