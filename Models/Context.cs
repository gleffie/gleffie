﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Gleefie_MVC.Models;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{

    public class ApplicationUser : IdentityUser
    {
        public string imagePath { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

    }
    public class Context : IdentityDbContext<ApplicationUser>
    {
        public Context()
            : base("GleefieContext")
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 240;
            Database.SetInitializer<Context>(null);
            this.Configuration.ProxyCreationEnabled = false;
        }

        public static Context Create()
        {
            return new Context();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
        public DbSet<UserProfileVO> UserProfiles { get; set; }
        public DbSet<CityVO> Cities { get; set; }
        public DbSet<CountryVO> Countries { get; set; }
        public DbSet<PermissionsVO> Permissions { get; set; }
        public DbSet<PubAddressesVO> PubAddresses { get; set; }
        public DbSet<PubsVO> Pubs { get; set; }
        public DbSet<RolesVO> UserRoles { get; set; }
        public DbSet<StatesVO> States { get; set; }
        public DbSet<EventTypeVO> Events { get; set; }
        public DbSet<LanguagesVO> Languages { get; set; }
        public DbSet<MediaTypeVO> MediaTypes { get; set; }
        public DbSet<PricingTypeVO> Pricings { get; set; }
        public DbSet<CategoryTypeVO> Categories { get; set; }
        public DbSet<SubCategoryTypeVO> SubCategories { get; set; }
        public DbSet<ArtistMediaVO> ArtistMediaType { get; set; }
        public DbSet<ArtistPaymentDetailsVO> ArtistPaymentDetails { get; set; }
        public DbSet<ArtistPerformanceDetailsVO> ArtistPerformanceDetailss { get; set; }
        public DbSet<ArtistProfileDetailsVO> ArtistProfileDetails { get; set; }

    }
}