﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{
    [Table("ArtistPaymentDetails")]
    public class ArtistPaymentDetailsVO
    {
        public ArtistPaymentDetailsVO()
        {
        }

        [Key()]
        public int Id { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountNumber { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string IFSCCode { get; set; }
        public string MICRCode { get; set; }
        public string PanNumber { get; set; }
        public Guid ProfileId { get; set; }

        [ForeignKey("ProfileId")]
        public ArtistProfileDetailsVO profileobj { get; set; }
        public DateTime? created { get; set; }
        public DateTime? modified { get; set; }
    }
}