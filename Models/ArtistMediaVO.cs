﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{
    [Table("ArtistMedia")]
    public class ArtistMediaVO
    {
        public ArtistMediaVO()
        {
        }

        [Key()]
        public Guid MediaId { get; set; }
        public Guid ProfileId { get; set; }
        public int MediaType { get; set; }

        [ForeignKey("MediaType")]
        public virtual MediaTypeVO MediaTypeObj { get; set; }
        public string MeidaURL { get; set; }
        public DateTime? created { get; set; }
        public DateTime? modified { get; set; }

        [ForeignKey("ProfileId")]
        public virtual ArtistProfileDetailsVO profileobj { get; set; }
    }
}