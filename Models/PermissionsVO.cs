﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;


namespace Gleefie_MVC.Models
{
    [Table("Permissions")]
    public class PermissionsVO
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string PermissionName { get; set; }
        public string PermissionDescription { get; set; }
        public DateTime? Created { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? Modifed { get; set; }
        public Guid? ModifiedBy { get; set; }
        public bool? isActive { get; set; }
       
        [ForeignKey("RoleId")]
        public RolesVO Role { get; set; }
    }
}