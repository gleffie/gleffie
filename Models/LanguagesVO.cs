﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Gleefie_MVC.Models
{
    [Table("Languages")]
    public class LanguagesVO
    {
        public LanguagesVO()
        {
        }

        [Key()]
        public int Id { get; set; }
        public string Language { get; set; }       
        public bool? isActive { get; set; }
    }
}