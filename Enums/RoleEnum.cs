﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Gleefie_MVC.Enums
{
    public enum RoleEnum
    {
        [Description("569C97C4-4B09-4729-80DE-62A4FBFAFE54")]
        User = 1,
        [Description("AE4F036B-D5E0-4769-8587-410C6DD49E9F")]
        Artist = 2,
        [Description("B4BC3C66-8C14-46F8-BCE0-86F9AEF9A9D1")]
        Pubs = 3,
        [Description("4C776C96-93BD-4213-9B13-67C86973E9E7")]
        Admin = 4,
        [Description("456FC511-BA1C-43C0-A304-930D739FF1AC")]
        Maintanance = 5        
    }
}